<?php
/**
 * Pdf Shipment Pickslip
 *
 * @package     Temando_Bin
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Bin_Model_Pdf_Shipment_Pickslip extends Mage_Sales_Model_Order_Pdf_Abstract
{
    /**
     * Address lines for header text
     *
     * @var array
     */
    protected $addressLines = array(
        '',
        '',
        ''
    );

    /**
     * Design config array if using a static file within the Temando
     * directory in adminhtml skin directory
     * @var array
     */
    protected $designConfig = array(
        '_area'     => 'adminhtml',
        '_package'  => 'default',
        '_theme'    => 'default'
    );

    /**
     * Total number of items contained in the master pickslip
     *
     * @var int
     */
    protected $totalItems = 0;

    /**
     * The barcode TT Font file path relative to current skin folder
     * ie: /skin/adminhtml/default/default/ + {path}
     */
    const TEMANDO_FONT = 'temando/barcode-fonts/FRE3OF9X.TTF';

    /**
     * The main TT Font file path relative to current skin folder
     * ie: /skin/adminhtml/default/default/ + {path}
     */
    const ARIAL_REG = 'temando/fonts/arial.ttf';
    const ARIAL_BOLD= 'temando/fonts/arialbd.ttf';

    /**
     * Draw table header for product items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Products'),
            'feed' => 165,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Location'),
            'feed'  => 35
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Qty'),
            'feed'  => 115
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('SKU'),
            'feed'  => 565,
            'align' => 'right'
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Draw table header for product items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawPackageHeader(Zend_Pdf_Page $page, $sku)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('SKU: ') . $sku,
            'feed' => 35,
            'align'=> 'left',
        );
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Packaging'),
            'feed' => 140,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Weight'),
            'feed'  => 240
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Length'),
            'feed'  => 340,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Width'),
            'feed'  => 440,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Height'),
            'feed'  => 560,
            'align' => 'right',
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 15
        );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 5;
    }

    /**
     * Return PDF document
     *
     * @param  array $shipments
     * @return Zend_Pdf
     */
    public function getPdf($shipments = array(), $masterPage = false)
    {
        // preprocess shipment status before generating the pdf
        foreach ($shipments as $shipment) {
            if (Mage::helper('temando')->canChangeShipmentStatus($shipment)) {
                $psGenerated = Mage::getModel('temandops/ps');
                /* @var $ps Temando_Ps_Model_Ps */
                $psGenerated->updatePsGenerated($shipment);
            }
        }
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        if ((count($shipments) > 1) && $masterPage) {
            $this->_createMasterPage($shipments);
        }

        foreach ($shipments as $shipment) {
            /* @var $shipment Temando_Temando_Model_Shipment */
            $order = $shipment->getOrder();
            if ($order->getStoreId()) {
                Mage::app()->getLocale()->emulate($order->getStoreId());
                Mage::app()->setCurrentStore($order->getStoreId());
            }
            $page  = $this->newPage();
            /* Add image */
            $this->insertLogo($page, $order->getStore());
            /* Add address */
            $this->insertAddress($page, $order->getStore());
            /* Add head */
            $this->insertOrder($page, $order, true, $order->getStoreId());
            /* Add document text and number */
            $this->insertDocumentNumber(
                $page,
                Mage::helper('temando')->__('PICK SLIP')
            );
            /* Add table */
            $this->_drawShipmentHeader($page);
            /* Add body */
            foreach ($shipment->getAllItems() as $shipmentItem) {
                /* @var $shipmentItem Temando_Temando_Model_Shipment_Item */
                $item = $shipmentItem->getOrderItem();
                /* @var $item Mage_Sales_Model_Order_Item */
                if ($item->getParentItem() || $item->getIsVirtual()) {
                    continue;
                }
                if ($item->getProduct() && $item->getProduct()->isVirtual()) {
                    continue;
                }

                /* Draw item */
                $this->_drawShipmentItem($shipmentItem, $page);
                $page = end($pdf->pages);
            }
            /* Add Packages */
            $this->processShipment($page, $pdf, $shipment);
        }
        $this->_afterGetPdf();
        if ($order->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }
        return $pdf;
    }

    /**
     * Create new page and assign to PDF object
     *
     * @param  array $settings
     * @return Zend_Pdf_Page
     */
    public function newPage(array $settings = array())
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
        return $page;
    }

    /**
     * Draw table header for shipment items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawShipmentHeader(Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Location'),
            'feed'  => 35
        );
        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Ordered'),
            'feed'  => 95
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('SKU'),
            'feed'  => 155
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Colour'),
            'feed'  => 250,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Size'),
            'feed'  => 300,
        );

        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Product'),
            'feed' => 350,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Shipped'),
            'feed'  => 565,
            'align' => 'right'
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Draw Item process
     *
     * @param  Temando_Temando_Model_Shipment_Item $shipmentItem
     * @param  Zend_Pdf_Page $page
     * @return Zend_Pdf_Page
     */
    protected function _drawShipmentItem(Temando_Temando_Model_Shipment_Item $shipmentItem, Zend_Pdf_Page $page)
    {
        $lines  = array();

        $item = $shipmentItem->getOrderItem();
        /* @var $item Mage_Sales_Model_Order_Item */

        $product = $item->getProduct();

        if ($product->isConfigurable()) {
            $product = Mage::helper('temando')
                ->getSelectedSimpleProductFromConfigurable($item);
        }

        // draw Product name
        $lines[0] = array(array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 60, true, true),
            'feed' => 350,
        ));

        // draw bin location
        $lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($product->getBinLocation(), 15),
            'feed'  => 35
        );

        // draw ordered QTY
        $lines[0][] = array(
            'text'  => $shipmentItem->getQtyOrdered(),
            'feed'  => 95
        );

        // draw shipped QTY
        $lines[0][] = array(
            'text'  => $shipmentItem->getQtyShipped(),
            'feed'  => 535
        );

        // draw SKU
        $lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 25),
            'feed'  => 155,
        );

        // Custom options
        $options = $this->getItemOptions($item);
        if ($options) {
            foreach ($options as $option) {
                if ((strip_tags($option['label']) != 'Size') && (strip_tags($option['label']) != 'Color')) {
                    continue;
                }
                switch ($option['label']) {
                    case 'Size' :
                        $feed = 301;
                        break;
                    case 'Color' :
                        $feed = 251;
                        break;
                    default :
                        continue;
                }

                // draw options value
                if ($option['value']) {
                    $_printValue = isset($option['print_value'])
                        ? $option['print_value']
                        : strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[0][] = array(
                            'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                            'feed' => $feed
                        );
                        break;
                    }
                }
            }
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 20
        );

        $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        return $page;
    }

    /**
     * Draw Item process
     *
     * @param  Varien_Object $item
     * @param  Zend_Pdf_Page $page
     * @param Mage_Sales_Model_Order $order
     * @return Zend_Pdf_Page
     */
    protected function _drawItem(Varien_Object $item, Zend_Pdf_Page $page, Mage_Sales_Model_Order $order)
    {
        $lines  = array();

        // draw Product name
        $lines[0] = array(array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 60, true, true),
            'feed' => 100,
        ));

        // draw QTY
        $lines[0][] = array(
            'text'  => $item->getQtyOrdered() ? $item->getQtyOrdered()*1 : $item->getQty(),
            'feed'  => 35
        );

        // draw SKU
        $lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 25),
            'feed'  => 565,
            'align' => 'right'
        );

        // Custom options
        $options = $this->getItemOptions($item);
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 70, true, true),
                    'font' => 'italic',
                    'feed' => 110
                );

                // draw options value
                if ($option['value']) {
                    $_printValue = isset($option['print_value'])
                        ? $option['print_value']
                        : strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[][] = array(
                            'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                            'feed' => 115
                        );
                    }
                }
            }
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 20
        );

        $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        return $page;
    }

    /**
     * Draw Package process
     *
     * @param  Varien_Object $item
     * @param  Zend_Pdf_Page $page
     * @param Mage_Sales_Model_Order $order
     * @return Zend_Pdf_Page
     */
    protected function _drawPackage(Varien_Object $item, Zend_Pdf_Page $page)
    {
        $lines  = array();
        $packages = Mage::helper('temando')->getProductArticles($item);
        $weight_type = Mage::getModel('temando/system_config_source_unit_weight')
            ->getOptionValue(
                Mage::helper('temando')->getConfigData('units/weight')
            );
        $measure_type = Mage::getModel('temando/system_config_source_unit_measure')
            ->getOptionValue(
                Mage::helper('temando')->getConfigData('units/measure')
            );

        for ($i=0; $i<count($packages); $i++) {
            //draw index
            $lines[$i] = array(array(
                'text' => sprintf("Package %d", $i+1),
                'feed' => 35,
                'align'=> 'left',
            ));
            // draw Product packaging
            $lines[$i][] = array(
                'text' => Mage::getModel('temando/system_config_source_shipment_packaging')
                    ->getOptionLabel($packages[$i]['packaging']),
                'feed' => 140,
            );
            // draw weight
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $packages[$i]['weight'], $weight_type),
                'feed'  => 240
            );
            // draw length
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $packages[$i]['length'], $measure_type),
                'feed'  => 340,
            );
            // draw width
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $packages[$i]['width'], $measure_type),
                'feed'  => 440,
            );
            // draw height
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $packages[$i]['height'], $measure_type),
                'feed'  => 560,
                'align'     => 'right',
            );
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        return $page;
    }

    /**
     * Return item Sku
     *
     * @param  $item
     * @return mixed
     */
    public function getSku($item)
    {
        if ($item->getProductOptionByCode('simple_sku')) {
            return $item->getProductOptionByCode('simple_sku');
        } else {
            return $item->getSku();
        }
    }

    /**
     * Retrieve item options
     *
     * @return array
     */
    public function getItemOptions($item)
    {
        $result = array();
        if ($options = $item->getProductOptions()) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }

    /**
     * Insert logo to pdf page
     *
     * @param Zend_Pdf_Page $page
     * @param null $store
     */
    protected function insertLogo(&$page, $store = null)
    {

        if (!Mage::helper('temando')->getConfigData('pickslip/logo')) {
            return;
        }
        $this->y = $this->y ? $this->y : 815;
        $image = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS .
            Temando_Temando_Model_System_Config_Backend_Image::UPLOAD_DIR . DS .
            Mage::helper('temando')->getConfigData('pickslip/logo_image');
        if (is_file($image)) {
            $image       = Zend_Pdf_Image::imageWithPath($image);
            $top         = 830; //top border of the page
            $widthLimit  = 270; //half of the page width
            $heightLimit = 270; //assuming the image is not a "skyscraper"
            $width       = $image->getPixelWidth();
            $height      = $image->getPixelHeight();

            //preserving aspect ratio (proportions)
            $ratio = $width / $height;
            if ($ratio > 1 && $width > $widthLimit) {
                $width  = $widthLimit;
                $height = $width / $ratio;
            } elseif ($ratio < 1 && $height > $heightLimit) {
                $height = $heightLimit;
                $width  = $height * $ratio;
            } elseif ($ratio == 1 && $height > $heightLimit) {
                $height = $heightLimit;
                $width  = $widthLimit;
            }

            $y1 = $top - $height;
            $y2 = $top;
            $x1 = 25;
            $x2 = $x1 + $width;

            //coordinates after transformation are rounded by Zend
            $page->drawImage($image, $x1, $y1, $x2, $y2);

            $this->y = $y1 - 10;
        }
    }

    /**
     * Insert address to pdf page
     *
     * @param Zend_Pdf_Page $page
     * @param null $store
     */
    protected function insertAddress(&$page, $store = null)
    {
        if (!Mage::helper('temando')->getConfigData('pickslip/address')) {
            return;
        }
        $this->addressLines = array (
            Mage::helper('temando')->getConfigData('pickslip/street'),
            Mage::helper('temando')->getConfigData('pickslip/suburb'),
            Mage::helper('temando')->getConfigData('pickslip/postcode')
        );
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 10);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = 815;
        foreach ($this->addressLines as $value) {
            if ($value !== '') {
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignRight($_value, 130, 440, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 10;
                }
            }
        }
        $this->y = ($this->y > $top) ? $top : $this->y;
    }



    /**
     * Insert order to pdf page
     *
     * @param Zend_Pdf_Page $page
     * @param Mage_Sales_Model_Order $obj
     * @param bool $putOrderId
     */
    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof Mage_Sales_Model_Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;

        $page->setFillColor(new Zend_Pdf_Color_Html("#FFFFFF"));
        $page->setLineColor(new Zend_Pdf_Color_Html("#000000"));
        $page->drawRectangle(25, $top, 570, $top - 55);
        $page->setFillColor(new Zend_Pdf_Color_Html("#000000"));
        $this->setDocHeaderCoordinates(array(25, $top, 570, $top - 55));
        $this->_setFontRegular($page, 10);

        if ($putOrderId) {
            $page->drawText(
                Mage::helper('temando')->__('Order # ') . $order->getRealOrderId(),
                35,
                ($top -= 30),
                'UTF-8'
            );
        }

        $font = Mage::getDesign()->getSkinBaseDir($this->designConfig).DS.self::TEMANDO_FONT;
        $page->setFont(Zend_Pdf_Font::fontWithPath($font), 48);
        $barcode = "*".$order->getRealOrderId()."*";
        $page->drawText($barcode, 200, $top -15);
        $this->_setFontRegular($page, 10);

        $page->drawText(
            Mage::helper('temando')->__('Order Date: ') . Mage::helper('core')->formatDate(
                $order->getCreatedAtStoreDate(),
                'medium',
                false
            ),
            35,
            ($top -= 15),
            'UTF-8'
        );

        $top -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $top, 275, ($top - 25));
        $page->drawRectangle(275, $top, 570, ($top - 25));

        /* Calculate blocks info */

        /* Billing Address */
        $billingAddress = $this->_formatAddress($order->getBillingAddress()->format('pdf'));

        /* Payment */
        $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
            ->setIsSecureMode(true)
            ->toPdf();
        $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key => $value) {
            if (strip_tags(trim($value)) == '') {
                unset($payment[$key]);
            }
        }
        reset($payment);

        /* Shipping Address and Method */
        if (!$order->getIsVirtual()) {
            /* Shipping Address */
            $shippingAddress = $this->_formatAddress($order->getShippingAddress()->format('pdf'));
            $shippingMethod  = $order->getShippingDescription();
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page, 12);
        $page->drawText(Mage::helper('temando')->__('Sold to:'), 35, ($top - 15), 'UTF-8');

        if (!$order->getIsVirtual()) {
            $page->drawText(Mage::helper('temando')->__('Ship to:'), 285, ($top - 15), 'UTF-8');
        } else {
            $page->drawText(Mage::helper('temando')->__('Payment Method:'), 285, ($top - 15), 'UTF-8');
        }

        $addressesHeight = $this->_calcAddressHeight($billingAddress);
        if (isset($shippingAddress)) {
            $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->drawRectangle(25, ($top - 25), 570, $top - 33 - $addressesHeight);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $this->y = $top - 40;
        $addressesStartY = $this->y;

        foreach ($billingAddress as $value) {
            if ($value !== '') {
                $text = array();
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $page->drawText(strip_tags(ltrim($part)), 35, $this->y, 'UTF-8');
                    $this->y -= 15;
                }
            }
        }

        $addressesEndY = $this->y;

        if (!$order->getIsVirtual()) {
            $this->y = $addressesStartY;
            foreach ($shippingAddress as $value) {
                if ($value!=='') {
                    $text = array();
                    foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
                        $text[] = $_value;
                    }
                    foreach ($text as $part) {
                        $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
                        $this->y -= 15;
                    }
                }
            }

            $addressesEndY = min($addressesEndY, $this->y);
            $this->y = $addressesEndY;

            $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $page->setLineWidth(0.5);
            $page->drawRectangle(25, $this->y, 275, $this->y-25);
            $page->drawRectangle(275, $this->y, 570, $this->y-25);

            $this->y -= 15;
            $this->_setFontBold($page, 12);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page->drawText(Mage::helper('temando')->__('Payment Method'), 35, $this->y, 'UTF-8');
            $page->drawText(Mage::helper('temando')->__('Shipping Method:'), 285, $this->y, 'UTF-8');

            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page, 10);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            $paymentLeft = 35;
            $yPayments   = $this->y - 15;
        } else {
            $yPayments   = $addressesStartY;
            $paymentLeft = 285;
        }

        foreach ($payment as $value) {
            if (trim($value) != '') {
                //Printing "Payment Method" lines
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
                    $page->drawText(strip_tags(trim($_value)), $paymentLeft, $yPayments, 'UTF-8');
                    $yPayments -= 15;
                }
            }
        }

        if ($order->getIsVirtual()) {
            // replacement of Shipments-Payments rectangle block
            $yPayments = min($addressesEndY, $yPayments);
            $page->drawLine(25, ($top - 25), 25, $yPayments);
            $page->drawLine(570, ($top - 25), 570, $yPayments);
            $page->drawLine(25, $yPayments, 570, $yPayments);

            $this->y = $yPayments - 15;
        } else {
            $topMargin    = 15;
            $methodStartY = $this->y;
            $this->y     -= 15;

            foreach (Mage::helper('core/string')->str_split($shippingMethod, 45, true, true) as $_value) {
                $page->drawText(strip_tags(trim($_value)), 285, $this->y, 'UTF-8');
                $this->y -= 15;
            }

            $yShipments = $this->y;
            $totalShippingChargesText = "(" . Mage::helper('temando')->__('Total Shipping Charges') . " "
                . $order->formatPriceTxt($order->getShippingAmount()) . ")";

            $page->drawText($totalShippingChargesText, 285, $yShipments - $topMargin, 'UTF-8');
            $yShipments -= $topMargin + 10;

            $instructions = '';
            $temandoShipments = Mage::getResourceModel('temando/shipment_collection')
                ->addFieldToFilter('order_id', $order->getId());
            if ($temandoShipments->count()) {
                $instructions = $temandoShipments->getFirstItem()->getShippingInstructions();
            }
            if (strlen(trim($instructions))) {
                $page->drawText(
                    Mage::helper('temando')->__('Shipping Instructions: ').$instructions,
                    285,
                    $yShipments - $topMargin,
                    'UTF-8'
                );
                $yShipments -= $topMargin + 10;
            }

            $tracks = array();
            if ($shipment) {
                $tracks = $shipment->getAllTracks();
            }
            if (count($tracks)) {
                $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                $page->setLineWidth(0.5);
                $page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
                $page->drawLine(400, $yShipments, 400, $yShipments - 10);
                //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

                $this->_setFontRegular($page, 9);
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                //$page->drawText(Mage::helper('temando')->__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
                $page->drawText(Mage::helper('temando')->__('Title'), 290, $yShipments - 7, 'UTF-8');
                $page->drawText(Mage::helper('temando')->__('Number'), 410, $yShipments - 7, 'UTF-8');

                $yShipments -= 20;
                $this->_setFontRegular($page, 8);
                foreach ($tracks as $track) {
                    $CarrierCode = $track->getCarrierCode();
                    if ($CarrierCode != 'custom') {
                        $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
                        $carrierTitle = $carrier->getConfigData('title');
                    } else {
                        $carrierTitle = Mage::helper('temando')->__('Custom Value');
                    }

                    //$truncatedCarrierTitle = substr($carrierTitle, 0, 35) . (strlen($carrierTitle) > 35 ? '...' : '');
                    $maxTitleLen = 45;
                    $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
                    $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen) . $endOfTitle;
                    //$page->drawText($truncatedCarrierTitle, 285, $yShipments , 'UTF-8');
                    $page->drawText($truncatedTitle, 292, $yShipments, 'UTF-8');
                    $page->drawText($track->getNumber(), 410, $yShipments, 'UTF-8');
                    $yShipments -= $topMargin - 5;
                }
            } else {
                $yShipments -= $topMargin - 5;
            }

            $currentY = min($yPayments, $yShipments);

            // replacement of Shipments-Payments rectangle block
            $page->drawLine(25, $methodStartY, 25, $currentY); //left
            $page->drawLine(25, $currentY, 570, $currentY); //bottom
            $page->drawLine(570, $currentY, 570, $methodStartY); //right

            $this->y = $currentY;
            $this->y -= 15;
        }
    }

    /**
     * Insert title and number for concrete document type
     *
     * @param  Zend_Pdf_Page $page
     * @param  string $text
     * @return void
     */
    public function insertDocumentNumber(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 35, $docHeader[1] - 15, 'UTF-8');
    }

    /**
     * Calculate address height
     *
     * @param  array $address
     * @return int Height
     */
    protected function _calcAddressHeight($address)
    {
        $y = 0;
        foreach ($address as $value) {
            if ($value !== '') {
                $text = array();
                foreach (Mage::helper('core/string')->str_split($value, 55, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $y += 15;
                }
            }
        }
        return $y;
    }

    /**
     * Draw the master page based on all the shipment items
     * quantity ordered
     *
     * @param array $shipments
     */
    protected function _createMasterPage(array $shipments)
    {
        $items = array();
        foreach ($shipments as $shipment) {
            /* @var $shipment Temando_Temando_Model_Shipment */
            $temandoShipmentItems = Mage::getResourceModel('temando/shipment_item_collection')
                ->addFieldToFilter('shipment_id', $shipment->getId());
            foreach ($temandoShipmentItems as $tmdShipmentItem) {
                /* @var $tmdShipmentItem Temando_Temando_Model_Shipment_Item */
                if (isset($items[$tmdShipmentItem->getSku()])) {
                    $items[$tmdShipmentItem->getSku()]['qty'] =
                        $items[$tmdShipmentItem->getSku()]['qty'] + $tmdShipmentItem->getQtyOrdered();
                } else {
                    $product = $tmdShipmentItem->getProduct();
                    if ($product->isConfigurable()) {
                        $product = Mage::helper('temando')
                            ->getSelectedSimpleProductFromConfigurable($tmdShipmentItem->getOrderItem());
                    }
                    $items[$tmdShipmentItem->getSku()] = array(
                        'qty' => $tmdShipmentItem->getQtyOrdered(),
                        'name' => $tmdShipmentItem->getProduct()->getName(),
                        'bin_location' => $product->getBinLocation()
                    );
                }
                $this->totalItems += $tmdShipmentItem->getQtyOrdered();
            }
        }
        if (!empty($shipments)) {
            $page  = $this->newPage();
            $this->insertLogo($page);
            $this->insertAddress($page);
            $this->insertMasterHeader($page);
            $this->insertDocumentNumber(
                $page,
                Mage::helper('temando')->__('MASTER PICK SLIP')
            );
            $this->_drawHeader($page);
            $this->_drawCombinedShipmentItems($items, $page);
        }
    }

    /**
     * Insert master header to pdf master page
     *
     * @param Zend_Pdf_Page $page
     */
    protected function insertMasterHeader(&$page)
    {
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;

        $page->setFillColor(new Zend_Pdf_Color_Html("#FFFFFF"));
        $page->setLineColor(new Zend_Pdf_Color_Html("#000000"));
        $page->drawRectangle(25, $top, 570, $top - 55);
        $page->setFillColor(new Zend_Pdf_Color_Html("#000000"));
        $this->setDocHeaderCoordinates(array(25, $top, 570, $top - 55));
        $this->_setFontRegular($page, 10);

        $date = Mage::app()->getLocale()->storeTimeStamp(Mage::app()->getStore()->getId());

        $page->drawText(
            Mage::helper('temando')->__('Order Items: ') . $this->totalItems,
            35,
            ($top -= 30),
            'UTF-8'
        );

        $page->drawText(
            Mage::helper('temando')->__('Date: ') . Mage::helper('core')->formatDate(
                date('Y-m-d', $date),
                'medium',
                false
            ),
            35,
            ($top -= 15),
            'UTF-8'
        );

        $top -= 10;
        $this->y = $top;

    }

    /**
     * Draw combined shipment items for the master page
     *
     * @param array $items
     * @param Zend_Pdf_Page $page
     * @return Zend_Pdf_Page
     */
    protected function _drawCombinedShipmentItems(array $items, Zend_Pdf_Page $page)
    {
        foreach ($items as $sku => $data) {
            $lines  = array();

            // draw Product name
            $lines[0] = array(array(
                'text' => $data['name'],
                'feed' => 165,
            ));

            // draw bin location
            $lines[0][] = array(
                'text'  => Mage::helper('core/string')->str_split($data['bin_location'], 15),
                'feed'  => 35
            );

            // draw QTY
            $lines[0][] = array(
                'text'  => $data['qty'],
                'feed'  => 115
            );

            // draw SKU
            $lines[0][] = array(
                'text'  => $sku,
                'feed'  => 565,
                'align' => 'right'
            );

            $lineBlock = array(
                'lines'  => $lines,
                'height' => 20
            );

            $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        }
        return $page;
    }

    /**
     * Draw table header for box items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function drawBoxHeader(Zend_Pdf_Page $page, $number)
    {
        if ($this->y < 100) {
            $page = $this->newPage(array('table_header' => true));
        }
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Package #') . $number,
            'feed' => 35,
            'align'=> 'left',
        );
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Package Type'),
            'feed' => 140,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Weight'),
            'feed'  => 240
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Length'),
            'feed'  => 340,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Width'),
            'feed'  => 440,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Height'),
            'feed'  => 560,
            'align' => 'right',
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 15
        );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 5;
        return $page;
    }

    /**
     * Draw Box process
     *
     * @param  Varien_Object $item
     * @param  Zend_Pdf_Page $page
     * @param Mage_Sales_Model_Order $order
     * @return Zend_Pdf_Page
     */
    protected function drawBox(Temando_Temando_Model_Box $box, Zend_Pdf_Page $page)
    {
        $lines  = array();
        $weightType = Mage::getModel('temando/system_config_source_unit_weight')
            ->getOptionValue($box->getWeightUnit());
        $measureType = Mage::getModel('temando/system_config_source_unit_measure')
            ->getOptionValue($box->getMeasureUnit());
        for ($i=0; $i<count($box); $i++) {
            //draw packaging description
            $name = $box->getComment();
            $lines[$i] = array(array(
                'text' => Mage::helper('core/string')->str_split($name, 20, true, true),
                'feed' => 35,
                'align'=> 'left',
            ));
            // draw box packaging
            $lines[$i][] = array(
                'text' => Mage::getModel('temando/system_config_source_shipment_packaging')
                    ->getOptionLabel($box->getPackaging()),
                'feed' => 140,
            );
            // draw weight
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $box->getWeight(), $weightType),
                'feed'  => 240
            );
            // draw length
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $box->getLength(), $measureType),
                'feed'  => 340,
            );
            // draw width
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $box->getWidth(), $measureType),
                'feed'  => 440,
            );
            // draw height
            $lines[$i][] = array(
                'text'  => sprintf('%.2f %s', $box->getHeight(), $measureType),
                'feed'  => 560,
                'align'     => 'right',
            );
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        return $page;
    }

     /**
     * Draw table header for article items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function drawArticleHeader(Zend_Pdf_Page $page)
    {
        if ($this->y < 30) {
            $page = $this->newPage(array('table_header' => true));
        }
        /* Add table head */
        $this->_setFontRegular($page, 10);
        //$page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setFillColor(new Zend_Pdf_Color_Rgb(255, 255, 255));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(130, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('temando')->__('Products'),
            'feed' => 240
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('SKU'),
            'feed'  => 140
        );

        $lines[0][] = array(
            'text'  => Mage::helper('temando')->__('Qty'),
            'feed'  => 560,
            'align' => 'right'
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 10;
    }

    /**
     * Draw Articles process
     *
     * @param  array $articlesPerBox
     * @param  Zend_Pdf_Page $page
     * @param Mage_Sales_Model_Order $order
     * @return Zend_Pdf_Page
     */
    protected function drawArticles($articlesPerBox, Zend_Pdf_Page $page)
    {
        $lines  = array();
        $articleSkus = array();
        $articles = array();
        if (is_array($articlesPerBox)) {
            foreach ($articlesPerBox as $article) {
                $articleSkus[] = $article['sku'];
                $qtys = array_count_values($articleSkus);
                $article['qty'] = $qtys[$article['sku']];
                $articles[$article['sku']] = $article;
            }
            $i = 0;
            foreach ($articles as $article) {
                //draw quantity
                $lines[$i] = array(array(
                    'text' => $article['qty'],
                    'feed' => 560,
                    'align'=> 'right',
                ));
                // draw product description
                $lines[$i][] = array(
                    'text' => $article['description'],
                    'feed' => 240,
                );
                // draw sku
                $lines[$i][] = array(
                    'text'  => $article['sku'],
                    'feed'  => 140,
                    'align' => 'left',
                );
                $i++;
            }
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );

        $page = $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        return $page;
    }

    protected function processShipment(Zend_Pdf_Page $page, $pdf, Temando_Temando_Model_Shipment $shipment)
    {
        $articles = $this->getArticles($shipment);
        $packageIndex = 1;
        foreach ($shipment->getBoxes() as $box) {
            /* @var $box Temando_Temando_Model_Box */
            $qty = $box->getQty();
            for ($i=1; $i<=$qty; $i++) {
                /* Add box header */
                $page = $this->drawBoxHeader($page, $packageIndex);
                $articlesPerBox = array();
                if (isset($articles[$packageIndex]) && !empty($articles[$packageIndex])) {
                    foreach ($articles[$packageIndex] as $article) {
                        if (is_array($article)) {
                            $articlesPerBox[] = array_key_exists(0, $article) ? $article[0] : $article;
                        }
                    }
                }
                $packageIndex++;
                $this->drawBox($box, $page);
                $page = end($pdf->pages);
                $this->drawArticleHeader($page);
                $page = end($pdf->pages);
                $this->drawArticles($articlesPerBox, $page);
                $this->y -= 20;
                $page = end($pdf->pages);
            }
        }
    }

    /**
     * Get articles for all ordered products within the shipment and
     * allocate them to boxes.  The original method uses qty to ship so it
     * doesn't allow products that have already been shipped to be displayed.
     *
     * @param Temando_Temando_Model_Shipment $shipment
     * @return array
     */
    protected function getArticles(Temando_Temando_Model_Shipment $shipment)
    {
        $articles = array();
        $origin = $shipment->getOrigin();
        foreach ($shipment->getAllItems() as $shipmentItem) {
            /* @var $shipmentItem Temando_Temando_Model_Shipment_Item */
            $item = $shipmentItem->getOrderItem();
            /* @var $item Mage_Sales_Model_Order_Item */
            if ($item->getIsVirtual()) {
                continue;
            }
            if ($item->getProduct() && $item->getProduct()->isVirtual()) {
                continue;
            }

            $packages = Mage::helper('temando')
                ->getProductArticles(
                    $item,
                    $origin->getCountry() != $shipment->getDestinationCountry()
                );
            for ($i=1; $i<=$shipmentItem->getQtyOrdered(); $i++) {
                foreach ($packages as $package) {
                    $articles[] = array(
                        'description' => $package['description'],
                        'sku' => $item->getSku(),
                        'goodsCurrency' => $shipment->getOrder()->getStore()->getCurrentCurrencyCode(),
                        'goodsValue' => $package['value']
                    );
                }
            }
        }
        Mage::register('temando_bypass_article_exception', true);
        $alocatedArticles = Mage::helper('temando')
            ->alocateArticlesToBoxesToShip($articles, $shipment->getBoxes());
        Mage::unregister('temando_bypass_article_exception');
        return $alocatedArticles;
    }

    /**
     * Set font as regular
     *
     * @param  Zend_Pdf_Page $object
     * @param  int $size
     * @return Zend_Pdf_Resource_Font
     */
    protected function _setFontRegular($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getDesign()->getSkinBaseDir().DS.self::ARIAL_REG);
        $object->setFont($font, $size);
        return $font;
    }

    /**
     * Set font as bold
     *
     * @param  Zend_Pdf_Page $object
     * @param  int $size
     * @return Zend_Pdf_Resource_Font
     */
    protected function _setFontBold($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getDesign()->getSkinBaseDir().DS.self::ARIAL_BOLD);
        $object->setFont($font, $size);
        return $font;
    }
}
