<?php
/**
 * Shipment Edit
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Block_Adminhtml_Shipment_Edit extends Temando_Temando_Block_Adminhtml_Shipment_Edit
{

    protected $_shipment;

    public function __construct()
    {
        parent::__construct();
        $add_button_method = 'addButton';
        if (!method_exists($this, $add_button_method)) {
            $add_button_method = '_addButton';
        }
        if ($this->getShipment()->getStatus()
            == Temando_Ps_Model_System_Config_Source_Shipment_Status::PS
        ) {
            $this->$add_button_method('getquote', array(
                'label' => Mage::helper('temando')->__('Save and Get Quotes'),
                'id' => 'getquote',
                'onclick' => 'saveAndGetQuotes()',
                'value' => '',
                'class' => 'save',
            ));
        }
    }
}
