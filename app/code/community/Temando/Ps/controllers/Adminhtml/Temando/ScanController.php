<?php
require_once Mage::getModuleDir('controllers', 'Temando_Temando').DS.'Adminhtml'.DS.'Temando'.DS.'ScanController.php';
/**
 * Admin Scan Controller
 *
 * @package     Temando_Temando
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Adminhtml_Temando_ScanController extends Temando_Temando_Adminhtml_Temando_ScanController
{

    /**
     * Pending pickslip action
     */
    public function pickslipAction()
    {
        $shipments = array();

        $shipmentCollection = Mage::getModel('temando/shipment')->getCollection();

        $shipmentCollection->join(
            'temando/quote',
            'main_table.admin_selected_quote_id=`temando/quote`.id',
            array(
                'magento_quote_id'
            )
        );
        //per user/warehouse view
        $currentUser = Mage::getSingleton('admin/session')->getUser();
        $allowedWarehouses = Mage::getModel('temando/warehouse')
            ->getCollection()
            ->getAllowedWarehouseIds($currentUser->getId());
        $shipmentCollection->addFieldToFilter('warehouse_id', array('in' => $allowedWarehouses));
        $shipmentCollection->addFieldToFilter('grid_display', 1);
        $shipmentCollection->addFieldToFilter('status', array(
            'nin' => array(
                Temando_Temando_Model_System_Config_Source_Shipment_Status::BOOKED
            )
        ));

        foreach ($shipmentCollection as $shipment) {
            if ($shipment->getId()) {
                $shipments[] = $shipment;
            }
        }

        if (!empty($shipments)) {
            try {
                $pdf = Mage::getModel('temando/pdf_shipment_pickslip')->getPdf($shipments, true);
                $this
                    ->_prepareDownloadResponse(
                        'pickslips-' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') . '.pdf',
                        $pdf->render(),
                        'application/pdf'
                    );
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()
                    ->addError(
                        Mage::helper('temando')->__(
                            'Error retrieving pick slips. More information in exception log.'
                        )
                    );
                $this->_redirect('*/*/');
            }
        } else {
            $this->_getSession()
                ->addError(
                    Mage::helper('temando')->__(
                        'No shipments available for pick slip retrieval.'
                    )
                );
            $this->_redirect('*/*/');
        }
        return;
    }
}
