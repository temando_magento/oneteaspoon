<?php
require_once Mage::getModuleDir('controllers', 'Temando_Temando').DS.'Adminhtml'.DS.'Temando'.DS.'ShipmentController.php';
/**
 * Admin Shipment Controller
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Adminhtml_Temando_ShipmentController
    extends Temando_Temando_Adminhtml_Temando_ShipmentController
{

    public function psAction()
    {
        $shipment = Mage::getModel('temando/shipment')->load($this->getRequest()->getParam('id'));
        /* @var $shipment Temando_Temando_Model_Shipment */
        if ($shipment->getId()) {
            try {
                if (Mage::helper('temando')->canChangeShipmentStatus($shipment)) {
                    $this->updatePsGenerated($shipment);
                    $this->_getSession()
                        ->addSuccess(
                            Mage::helper('temando')->__(
                                'Status has been updated for Order # ' . $shipment->getOrderNumber()
                            )
                    );
                } else {
                   $this->_getSession()
                        ->addError(
                            Mage::helper('temando')->__(
                                'Order # ' . $shipment->getOrderNumber() .  ' cannot be changed.'
                            )
                    );
                }

            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()
                    ->addError(
                        Mage::helper('temando')->__(
                            'Error changing the shipment status. More information in exception log.'
                        )
                    );
            }
        } else {
            $this->_getSession()->addError(
                Mage::helper('temando')->__(
                    'No shipment found.'
                )
            );
        }
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
    }


    /**
     * Mass pickslip action
     */
    public function massPsAction()
    {
        $params = $this->getRequest()->getParams();
        if (!isset($params['massaction']) || !is_array($params['massaction']) || empty($params['massaction'])) {
            $this->_getSession()->addError($this->__('Please select at least one shipment.'));
            $this->_redirect('*/*/');
        }
        $shipmentIds = $params['massaction'];
        $shipments = array();
        foreach ($shipmentIds as $shipmentId) {
            $shipment = Mage::getModel('temando/shipment')->load($shipmentId);
            /* @var $shipment Temando_Temando_Model_Shipment */
            if ($shipment->getId()) {
                $shipments[] = $shipment;
            }
        }
        if (!empty($shipments)) {
            try {
                foreach ($shipments as $shipment) {
                    if (Mage::helper('temando')->canChangeShipmentStatus($shipment)) {
                        $this->updatePsGenerated($shipment);
                    } else {
                       $this->_getSession()
                            ->addError(
                                Mage::helper('temando')->__(
                                    'Order # ' . $shipment->getOrderNumber() .  ' cannot be changed.'
                                )
                        );
                    }
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()
                    ->addError(
                        Mage::helper('temando')->__(
                            'Error changing the shipment status. More information in exception log.'
                        )
                    );
                $this->_redirect('*/*/');
            }
        } else {
            $this->_getSession()->addError(
                Mage::helper('temando')->__(
                    'No shipments selected for shipment status change.'
                )
            );
            $this->_redirect('*/*/');
        }
        $this->_redirect('*/*/');
        return;
    }

    /**
     * Process the changing of the PS generated status
     *
     * @param Temando_Temando_Model_Shipment $shipment
     */
    protected function updatePsGenerated($shipment)
    {
        $psGenerated = Mage::getModel('temandops/ps');
        /* @var $ps Temando_Ps_Model_Ps */
        $psGenerated->updatePsGenerated($shipment);
    }
}
