<?php
/**
 * Helper Data
 * Overwrites Temando Temando Helper Data
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Helper_Data extends Temando_Temando_Helper_Data
{

    public function canChangeShipmentStatus(Temando_Temando_Model_Shipment $shipment)
    {
        switch ($shipment->getStatus()) {
            case Temando_Temando_Model_System_Config_Source_Shipment_Status::PENDING:
                return true;
            default:
                return false;
        }
    }
}
