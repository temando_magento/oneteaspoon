<?php
/**
 * System Config Source Shipment Status
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Model_System_Config_Source_Shipment_Status extends Temando_Temando_Model_System_Config_Source_Shipment_Status
{

    const PENDING =     '0';
    const PART_BOOKED = '5';
    const BOOKED =      '10';
    const PS =          '2';

    protected function _setupOptions()
    {
        $this->_options = array(
            self::PENDING     => Mage::helper('temando')->__('Pending'),
            self::PS          => Mage::helper('temando')->__('PS Generated'),
            self::PART_BOOKED => Mage::helper('temando')->__('Partially Booked'),
            self::BOOKED      => Mage::helper('temando')->__('Booked'),
        );
    }
}
