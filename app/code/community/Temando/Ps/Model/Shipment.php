<?php
/**
 * Shipment Model
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 *
 */
class Temando_Ps_Model_Shipment extends Temando_Temando_Model_Shipment
{
    /**
     * Checks wether this shipment can be shipped
     *
     * @return boolean
     */
    public function isStatusOpened()
    {
        return
            $this->getStatus() == Temando_Temando_Model_System_Config_Source_Shipment_Status::PENDING ||
            $this->getStatus() == Temando_Temando_Model_System_Config_Source_Shipment_Status::PART_BOOKED ||
            $this->getStatus() == Temando_Ps_Model_System_Config_Source_Shipment_Status::PS;
    }

    /**
     * Process makeBooking request response
     *
     * @param stdClass $resultXml
     * @param Temando_Temando_Model_Quote $quote
     * @return void
     * @throws Exception $e
     */
    public function processBookingResult(stdClass $resultXml, Temando_Temando_Model_Quote $quote)
    {
        if ($this->getStatus()
            == Temando_Ps_Model_System_Config_Source_Shipment_Status::PS
        ) {
            $this->setStatus(Temando_Temando_Model_System_Config_Source_Shipment_Status::PENDING);
        }
        return parent::processBookingResult($resultXml, $quote);
    }
}
