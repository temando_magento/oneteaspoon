<?php
/**
 * Ps Model
 *
 * @package     Temando_Ps
 * @author      Temando Magento Team <marketing@temando.com>
 */
class Temando_Ps_Model_Ps extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
    }

    /**
     * Send an email as defined in the config
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function sendTransactional(Mage_Sales_Model_Order $order)
    {
        if (!Mage::helper('temando')->getConfigData('psemail/active')) {
            return;
        }

        $templateId = Mage::helper('temando')->getConfigData('psemail/template');

        if (!is_numeric($templateId) || !$order->getId()) {
            return;
        }

        try {
            //send email
            $storeId = $order->getStoreId();
            $store = Mage::getModel('core/store')->load($storeId);
            $order->setCustomerName($order->getCustomerFirstname() . ' ' . $order->getCustomerLastname());

            $mailer = Mage::getModel('core/email_template_mailer');
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo(
                $order->getCustomerEmail(),
                $order->getCustomerName()
            );
            $mailer->addEmailInfo($emailInfo);

            // Set all required params and send email
            $mailer->setSender(
                Mage::getStoreConfig('sales_email/order/identity'),
                $storeId
            );
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId($templateId);
            $mailer->setTemplateParams(
                array(
                    'order' => $order,
                    'store' => $store,
                    'customerName' => $order->getCustomerFirstname(),
                    'customerEmail' => $order->getCustomerEmail(),
                )
            );
            $mailer->send();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Process the changing of the PS generated status
     *
     * @param Temando_Temando_Model_Shipment $shipment
     */
    public function updatePsGenerated($shipment)
    {
        /* @var $ps Temando_Ps_Model_Ps */
        $order = $shipment->getOrder();
        $order->setIsInProcess(true)->save();
        $shipment
            ->setStatus(Temando_Ps_Model_System_Config_Source_Shipment_Status::PS)
            ->save();
        $this->sendTransactional($order);
    }
}
